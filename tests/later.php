<?php

function test_one() {

	$test_html      = load_html_file( __DIR__ . '/test1.html' );
	$correct_result = load_html_file( __DIR__ . '/test1-result.html' );

	$result = apply_filters( 'the_content', $test_html );
	$result = remove_whitespace( $result );

	$this->assertEquals( $result, $correct_result, $result );
}

function load_html_file( $string ) {
	// phpcs:ignore WordPress.WP.AlternativeFunctions.file_get_contents_file_get_contents
	$html = file_get_contents( $path );
	$html = remove_whitepace( $html );
}

function remove_whitespace( $string ) {
	return preg_replace( '/\s+/', '', $string );
}


function test_lazyload_adder( $content ) {

	$content .=
	'<figure><img src="https://placekitten.com/800/800"></figure>
	<img src="https://placekitten.com/500/800">
	<img src="https://placekitten.com/400/800">
	<img src="https://placekitten.com/300/100">
	<iframe src="https://www.youtube.com/embed/K3cjemVo0Go" width="250" height="500"></iframe>
	<iframe src="https://example.com" width="300" height="100"></iframe>';

	$content .=
	'<picture>
		<source
			media="(min-width: 40em)"
			srcset="
				simpleimage.huge.jpg 1x,
				simpleimage.huge.2x.jpg 2x
			"
		/>
		<source
			srcset="
				simpleimage.jpg 1x,
				simpleimage.2x.jpg 2x
			"
		/>
		<img
			src="simpleimage.jpg"
			loading="lazy"
			alt=".."
			width="250"
			height="150"
		/>
		<source
			media="(min-width: 40em)"
			srcset="
				simpleimage.huge.jpg 1x,
				simpleimage.huge.2x.jpg 2x
			"
		/>
		<source
			srcset="
				simpleimage.jpg 1x,
				simpleimage.2x.jpg 2x
			"
		/>
	</picture>';
}

<?php

class Tests_All extends WP_UnitTestCase {

	public function test_img_figure_img() {

		ob_start();
		?>

		<img src="img.jpg">
		<figure>
			<img src="figure-img.png">
		</figure>​

		<?php
		$test_html = $this->prepare_test_html( ob_get_clean() );
		ob_start();
		?>

		<img src="img.jpg" loading="eager">
		<figure>
			<noscript class="loading-lazy"><img src="figure-img.png" loading="lazy"></noscript>
		</figure>

		<?php
		$compare_html = $this->whitespace( ob_get_clean() );
		$this->assertEquals( $compare_html, $test_html, wp_json_encode( "$test_html ::: $compare_html" ) );
	}

	public function test_picture_iframe() {

		ob_start();
		?>

		<picture>
			<source srcset="source1.png" media="(min-width: 100px)">
			<source srcset="source2.png" media="(min-width: 200px)">
			<img src="src.png">
		</picture>
		<picture>
			<source srcset="source1.png" media="(min-width: 100px)">
			<source srcset="source2.png" media="(min-width: 200px)">
			<img src="src.png" loading="lazy"/>
			<source srcset="source3.png" media="(min-width: 300px)">
			<source srcset="source4.png" media="(min-width: 400px)">
		</picture>
		<iframe src="iframe.html" loading="lazy"></iframe>

		<?php
		$test_html = $this->prepare_test_html( ob_get_clean() );
		ob_start();
		?>

		<picture>
			<source srcset="source1.png" media="(min-width: 100px)">
			<source srcset="source2.png" media="(min-width: 200px)">
			<img src="src.png" loading="eager">
		</picture>
		<picture><noscript class="loading-lazy">
			<source srcset="source1.png" media="(min-width: 100px)">
			<source srcset="source2.png" media="(min-width: 200px)">
			<img src="src.png" loading="lazy">
			<source srcset="source3.png" media="(min-width: 300px)">
			<source srcset="source4.png" media="(min-width: 400px)">
		</noscript></picture>
		<noscript class="loading-lazy"><iframe src="iframe.html" loading="lazy"></iframe></noscript>

		<?php
		$compare_html = $this->whitespace( ob_get_clean() );
		$this->assertEquals( $compare_html, $test_html, wp_json_encode( "$test_html ::: $compare_html" ) );
	}

	public function prepare_test_html( $html ) {

		remove_filter( 'the_content', 'wpautop' );
		// Yes TWO times to test that nothing gets wrapper twice!
		$html = apply_filters( 'the_content', $html );
		$html = apply_filters( 'the_content', $html );
		$html = $this->whitespace( $html );
		return $html;
	}

	public function whitespace( $str ) {

		foreach (
			[
				'figure',
				'noscript',
				'div',
				'picture',
				'source',
				'span',
				'img',
			]
			as $tag
		) {
			$str = str_replace( "<$tag", " <$tag", $str );
			$str = str_replace( "</$tag>", " </$tag> ", $str );
		}

		$str = str_replace( [ "\r", "\n", "\t" ], ' ', $str );
		$str = preg_replace( '/\s+/', ' ', $str );
		$str = preg_replace( '/[\x{200B}]/u', '', $str );
		return trim( $str );
	}
}
